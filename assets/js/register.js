import Vue from 'https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.esm.browser.js';

new Vue({
  el: '#root',
  data() {
    return {
      name: '',
      password: '',
      confirm: '',
      email: '',
      login: '',
      errors: null,
    };
  },
  methods: {
    async register() {
      this.errors = '';
      const formData = new FormData();
      formData.append('login', this.login);
      formData.append('password', this.password);
      formData.append('confirm', this.confirm);
      formData.append('email', this.email);
      formData.append('name', this.name);
      try {
        const res = await fetch('vendor/signup.php', {
          method: 'POST',
          body: formData,
        });
        const data = await res.json();
        if (!data.status) {
          this.errors = data.message;
        }
        console.log(data);
      } catch (e) {
        console.error(e);
      }
      if (!this.errors) {
        document.location.pathname = '/profile.php';
      }
    },
  },
});
