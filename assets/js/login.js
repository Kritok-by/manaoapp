import Vue from 'https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.esm.browser.js';

new Vue({
  el: '#root',
  data() {
    return {
      login: '',
      password: '',
      errors: null,
    };
  },
  methods: {
    async logIn() {
      this.errors = null;
      const formData = new FormData();
      formData.append('login', this.login);
      formData.append('password', this.password);
      try {
        const res = await fetch('vendor/signin.php', {
          method: 'POST',
          body: formData,
        });
        const data = await res.json();
        if (!data.status) {
          this.errors = data.message;
        }
        console.log(data);
      } catch (e) {
        new Error(e);
      }
      // if (!this.errors) {
      //   document.location.pathname = '/profile.php';
      // }
    },
  },
});
