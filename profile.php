<?php
session_start();
if (!$_SESSION['user']) {
    header('Location: /');
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <title>Profile</title>
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>
    <div id="root">
            <h1 style="margin: 10px 0;">Hello <span style="color: #26a69a;"><?= $_SESSION['user']['name'] ?></span></h1>
            <a href="vendor/logout.php" class="waves-effect waves-light btn">Logout</a>
    </div>
</body>
</html>
