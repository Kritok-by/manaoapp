<?php
session_start();

if ($_SESSION['user']['id']===$_COOKIE['id']) {
    header('Location: profile.php');
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MANAO авторизация</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>
    <div id="root" v-cloak>
        <form @submit.prevent='logIn'>
        <div class="input-field col s6">
          <input id="last_name" type="text" class="validate" v-model='login'>
          <label for="last_name">Login</label>
        </div>
        <div class="input-field col s12">
          <input id="password" type="password" class="validate" v-model='password'>
          <label for="password">Password</label>
        </div>
        <p class="collection-item" v-if="errors">{{errors}}</p>
        <button class="btn waves-effect waves-light" type="submit" >Log In
    <i class="material-icons right">send</i>
  </button>
  <p>
      If you not have an account <a href="/register.php">register</a>!
    </p>
</form>
    </div>
    <script src="./assets/js/login.js" type='module'></script>

</body>
</html>
