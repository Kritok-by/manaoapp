<?php
session_start();
require_once './hash.php';

$login = $_POST['login'];
$password = $_POST['password'];


$password = hashPass($password);
$db = '../db.xml';
$connect = simplexml_load_file($db);
$users = $connect->root->user;
$user = [];
for($i=0;$i<count($users);$i++){
    if($users[$i]->login == $login and $users[$i]->password == $password){
      $email = $users[$i]->email;
      $name = $users[$i]->name;
      $login = $users[$i]->login;
      $id = $users[$i]->id;
      $user['email'] = "$email";
      $user['name'] = "$name";
      $user['login'] = "$login";
      $user['id'] = "$id";
    }
  }
  if(count($user) > 0){
    setcookie('id', $user['id'], time()+30*24*60*60, '/');
    $_SESSION['user'] = [
        "id" => $user['id'],
        "name" => $user['name'],
        "login" => $user['login'],
        "email" => $user['email']
    ];
    $response = [
        "status" => true
    ];
    echo json_encode($_SESSION['user']['id']);
  } else {
    $response = [
        "status" => false,
        "message" => 'Wrong login or password!'
    ];
    echo json_encode($response);
  }
?>
