<?php

session_start();
require_once './hash.php';

$name = $_POST['name'];
$login = $_POST['login'];
$email = $_POST['email'];
$password = $_POST['password'];
$confirm = $_POST['confirm'];
$db = '../db.xml';
$connect = simplexml_load_file($db);
$root = $connect->root;
$errors = [];

if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
    $errors['email'][]='Email is not correct!';}
  if (strlen($login) < 6){
    $errors['login'][]='Login must contain at least 6 letters!';}
  if (!preg_match("/^[a-zA-Z0-9]+$/", $login)){
    $errors['login'][]='Login should only consist of letters or numbers!';}
  if (strlen($password) < 6){
    $errors['password'][]='Password must contain at least 6 letters!';}
  if (!preg_match("#[0-9]+#", $password)){
    $errors['password'][]='Password must include at least one number!';}
  if (!preg_match("#[a-z]+#", $password)){
    $errors['password'][]='Password must include at least one letter!';}
  if (!preg_match("#[A-Z]+#", $password)){
    $errors['password'][]='Password must include at least one CAPS!';}
  if (!preg_match("#\W+#", $password)){
    $errors['password'][]='Password must include at least one symbol!';}
  if (strlen($name) < 2){
    $errors['name'][]='Name must contain at least 2 letters!';}
  if (!preg_match("/^[a-zA-Z0-9]+$/", $name)){
    $errors['name'][]='Name should only consist of letters or numbers!';}

    for($i=0;$i<count($root->user);$i++){
        if($root->user[$i]->email == $email){
          $errors['email'][]='User with this email is registered!';
        }
      }
      for($i=0;$i<count($root->user);$i++){
        if($root->user[$i]->login == $login){
          $errors['login'][]='User with this login is registered!';
        }
      }
      if(!$errors['password']){
        if($password!==$confirm){
          $errors['confirm'][]='Passwords do not match!';
        }
      }

if(count($errors)>0){
    $response = [
        "status" => false,
        "type" => 1,
        "message" => $errors,
    ];

    echo json_encode($response);
    die();
} else {
    addUser($_POST, $root);
    $connect->asXML($db);
    $response = [
        "status" => true,
        "message" => "Регистрация прошла успешно!",
    ];

    echo json_encode($response);
}

function addUser($data, $root){
    $hash = hashPass($data['password']);
    $user = $root->addChild('user');
    $id = uniqid($data['login']);
    $user->addChild('id', $id);
    $user->addChild('name', $data['name']);
    $user->addChild('login', $data['login']);
    $user->addChild('password', $hash);
    $user->addChild('email', $data['email']);
    setcookie('id', $id, time()+30*24*60*60, '/');
    $_SESSION['user'] = [
        "id" => $id,
        "name" => $data['name'],
        "login" => $data['login'],
        "email" => $data['email']
    ];

    }

?>
