<?php
    session_start();
    if ($_SESSION['user']['id']===$_COOKIE['id']) {
        header('Location: profile.php');
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <title>MANAO register</title>
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>
    <div id="root" v-cloak>
        <form @submit.prevent='register'>
        <div class="input-field col s6">
          <input id="login" type="text" class="validate" v-model='login'>
          <label for="login">Login</label>
        </div>
        <div v-if="errors">
            <p class="collection-item" v-for="(item, index) in errors.login" :key="index">
                {{item}}
            </p>
        </div>
        <div class="input-field col s12">
          <input id="password" type="password" class="validate" v-model='password'>
          <label for="password">Password</label>
        </div>
        <div v-if="errors">
            <p class="collection-item" v-for="(item, index) in errors.password" :key="index">
                {{item}}
            </p>
        </div>
        <div class="input-field col s12">
          <input id="password" type="password" class="validate" v-model='confirm'>
          <label for="password">Confirm password</label>
        </div>
        <div v-if="errors">
            <p class="collection-item" v-for="(item, index) in errors.confirm" :key="index">
                {{item}}
            </p>
        </div>
        <div class="input-field col s12">
          <input id="email" type="email" class="validate" v-model='email'>
          <label for="email">Email</label>
        </div>
        <div v-if="errors">
            <p class="collection-item" v-for="(item, index) in errors.email" :key="index">
                {{item}}
            </p>
        </div>
        <div class="input-field col s6">
          <input id="last_name" type="text" class="validate" v-model='name'>
          <label for="last_name">Name</label>
        </div>
        <div v-if="errors">
            <p class="collection-item" v-for="(item, index) in errors.name" :key="index">
                {{item}}
            </p>
        </div>
        <button class="btn waves-effect waves-light" type="submit" >Register
    <i class="material-icons right">send</i>
  </button>
  <p>
  If you have an account <a href="/">sign in</a>!
</p>
</form>
    </div>
    <script src="assets/js/register.js" type='module'></script>
</body>
</html>
